# libguestfs

This is a rebuild of libguestfs to avoid memory corruption within a koji build of docker images

Essentially koji relies on libdnf which uses the json-c library, and libguestfs which uses the jansson library. The current versions of these libraries do not use versioned symbols and have conflicting names

The rebuild is an older version of libguestfs that uses the 'yajl' library, which does not appear to have conflicting API names.

Without this version of libguestfs, koji fails when building a docker image with the error:
`free() invalid pointer`

The upstream bug of libguestfs is tracked here: https://bugzilla.redhat.com/show_bug.cgi?id=1923971

Once the above bug is fixed, this repository/package should be retired.
