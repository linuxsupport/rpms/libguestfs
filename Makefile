SPECFILE             = $(shell find -maxdepth 1 -type f -name *.spec)
SPECFILE_NAME        = $(shell awk '$$1 == "Name:"     { print $$2 }' $(SPECFILE) )
SPECFILE_VERSION     = $(shell awk '$$1 == "Version:"  { print $$2 }' $(SPECFILE) )
DIST                ?= $(shell rpm --eval %{dist})


sources:
	tar -zcvf $(SPECFILE_NAME)-$(SPECFILE_VERSION).tgz --exclude-vcs --transform 's,^code/,$(SPECFILE_NAME)-$(SPECFILE_VERSION)/,' code/*

rpm: sources
	rpmbuild -bb --define "dist $(DIST)" --define "_topdir $(PWD)/build" --define '_sourcedir $(PWD)/sources' $(SPECFILE)

srpm: sources
	rpmbuild -bs --define "dist $(DIST)" --define "_topdir $(PWD)/build" --define '_sourcedir $(PWD)/sources' $(SPECFILE)
